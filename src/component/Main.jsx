import React, { useState, useEffect } from "react";
import axios from 'axios';
import Modal from 'react-modal';
import "../styles/main.css";

const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)',
    background            :'grey',
    color                 :'white'
  }
};
const Main = () => {
  const [userName, setUserName] = useState([]);
  const [time, setTime] = useState([{start_time:{},end_time:{}}]);
  const [modalIsOpen,setIsOpen] = useState(false);

// modal
 const openModal=()=> {
    setIsOpen(true);
  }
   const closeModal=()=>{
    setIsOpen(false);
  }

// integration for user name
useEffect (()=>{
    axios
    .get('https://run.mocky.io/v3/dcbbfecc-9efe-4a6b-b87f-a49fb8548f96')
    .then(res=>{
        setUserName(res.data)
    })
    .catch(err=>{
        console.log(err)
    })   
})

//integration for getting time
useEffect (()=>{
    axios
    .get('https://run.mocky.io/v3/796bb8f1-aa43-4703-a4aa-5c553e396ce6')
    .then(res=>{
        setTime(res.data)
    })
    .catch(err=>{
        console.log(err)
    })   
})

  return (
    <div className="mainContainer">
      <div className="header">User List</div>
      <div>{userName.username}</div>
       {userName.map( (item,i )=> (
           <div className="userList" key={i}>
             <button key={i} onClick={openModal}>{item.username}</button>
            </div>
           ))}
        <Modal
          isOpen={modalIsOpen}
          onRequestClose={closeModal}
          style={customStyles}
        >
         <div className="time">START_TIME:  {time.start_time}</div>
         <div className="time">END_TIME:  {time.end_time}</div>
          <button onClick={closeModal} className="close">close</button>
        </Modal>
    </div>
  );
};
export default Main;

